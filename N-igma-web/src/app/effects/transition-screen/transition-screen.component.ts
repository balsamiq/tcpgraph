import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-transition-screen',
  templateUrl: './transition-screen.component.html',
  styleUrls: ['./transition-screen.component.css']
})
export class TransitionScreenComponent implements OnInit {

  transition:boolean = true;

  constructor() {}

  ngOnInit(): void {
    setTimeout( () => {
      this.transition = false;
    }, 1000);

  }

}
