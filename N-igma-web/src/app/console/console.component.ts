import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-console',
  templateUrl: './console.component.html',
  styleUrls: ['./console.component.css']
})
export class ConsoleComponent implements OnInit {

  network_status:string = "Online";
  last_incoming_msg:Number = 0;
  username:string = "TR4B";
  com_logs:Array<object> = [];
  

  constructor() {
    this.com_logs = [
      {
        origin:"GSD2",
        message:"LOREM IPSUM -OVER-"
      },
      {
        origin:"TR4B",
        message:"TRUE WHILE FALSE -STOP- BUT FALSE WHEN TRUE -OVER-"
      },
      {
        origin:"GSD2",
        message:"AHOY -OVER-"
      },
      {
        origin:"TR4B",
        message:"MINOR ERRORS -OVER-"
      },
    ]
  }

  ngOnInit(): void {
  }

}
