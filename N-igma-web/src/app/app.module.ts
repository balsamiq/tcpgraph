import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ConsoleComponent } from './console/console.component';
import { SplashScreenComponent } from './effects/splash-screen/splash-screen.component';
import { SettingsComponent } from './settings/settings.component';
import { EncryptionComponent } from './encryption/encryption.component';
import { TransitionScreenComponent } from './effects/transition-screen/transition-screen.component';

@NgModule({
  declarations: [
    AppComponent,
    ConsoleComponent,
    SplashScreenComponent,
    SettingsComponent,
    EncryptionComponent,
    TransitionScreenComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
