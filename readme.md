# TCPGraph
## _A Morse-based instant messaging platform_

This is meant to be a sort of a -several week long- ‘weekend project’, carried out in my spare time. It isn’t meant to be something useful nor optimal, I’m making it with the only purpose of testing different sorts of UI display and, ofcourse, for the fun of it.
It is overengineered on purpose.

The UI is based on DOS’s commonly seen GUI (see example). Trying to give an overall aspect, and transition effects from +30 years ago by using today’s CSS/HTML may be harder than it actually looks.

![Alt text](readme-images/sample.png?raw=true "Example")

I have in mind to use the following technologies during the development of this project:

- Design and implementation of vector based images.
- Animation of vectorized images.
- HTML/CSS transition effects.
- Fancy encryption method(*).


*Encryption will be based on Enigma’s electromechanical codification, by replicating it’s rotor-based behaviour.
The original idea was to rely only on morse inputs to transmit and receive, with an optional end-to-end character decryption. But due to it's obvious impracticality, this is no the case anymore. Now the intention is to let the user decide between input types: _telegraph key_ (by combining -/. or short/long press at spacebar) or keyboard.
All communications will be sent and received using only stringified morse standards, therefore, if the chosen input type is keyboard, those messages will be coded to morse before being sent via TCP.

Once received, all messages will be reproduced character by character, and written down at the chat box at the same time as every character is reproduced.


## To do
### Frontend:
- User overlay (Chat-room tab, general-settings tab, encryption-settings tab) [WIP]
- Able to send comms.
- Switch input type.
- Morse-input type (Length-push sensitive, pause between characters/words, settable wpm) [WIP]
- Encryption/Decryption.

### Backend:
- Messages stored in memory. Message history up to 10 messages.
- Api-rest based flow.


## Design wireframe
![Alt text](readme-images/TCPGraph.png?raw=true "Wireframe")
